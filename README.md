# HD Video Series - What is ....?

# About
I get a lot of questions about how to compile Android and how to make custom kernels, so I thought I would put together a video series with everything I know. (Don't worry, that will not take long!) Sometimes, when you make a custom rom or kernel, you need an app to run a command that has root permission.

# Who is this course for?
This course is for those who are able to flash custom recoveries, like TRWP or CWM, and who can root their phones or flash custom roms. Now that you can do those things, you are ready to start building your very own custom roms and kernels!

# What do we cover?
Here we trying to understand a few things about Android, from lingo to hardware, we are attempting to answer some basic and not so basic questions so we can have a better knowledge base to work on our Android projects.

